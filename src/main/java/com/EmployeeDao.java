package com;

import java.util.List;

public interface EmployeeDao {

    public void saveEmployee(EmployeeDetails employee);
    public List<EmployeeDetails> showAllEmployee();
    public void updateEmployee(int id,String name,String empNumber);
    public void deleteEmployee(EmployeeDetails employee);

}
