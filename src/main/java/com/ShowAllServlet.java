package com;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
@WebServlet("/ShowAllServlet")
public class ShowAllServlet extends HttpServlet {
    EmployeeDaoImp employeeDaoImp=new EmployeeDaoImp();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        {
            PrintWriter printWriter=resp.getWriter();
            resp.setContentType("text/html");
            printWriter.println("The list of the employees is:<br>");
            List<EmployeeDetails> employeeDetailsList=new ArrayList();
            employeeDetailsList=employeeDaoImp.showAllEmployee();
            printWriter.println("<center><table border=2px><th>EMPLOYEE ID</th><th>EMPLOYEE NAME</th><th>EMPLOYEE NUMBER</th>");
            for (EmployeeDetails e:employeeDetailsList) {

                printWriter.println("<tr><td>"+e.getEmployeeId()+"</td><td>"+e.getEmpName()+"</td><td>"+e.getEmpNumber()+"</td></tr>");
            }

            printWriter.println("</table></center>");
            RequestDispatcher rd = req.getRequestDispatcher("show.html");
            rd.include(req, resp);

        }

    }
}
