package com;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/UpdateEmployeeServlet")
public class UpdateEmployeeServlet extends HttpServlet {

    EmployeeDaoImp employeeDaoImp=new EmployeeDaoImp();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter printWriter=resp.getWriter();
        resp.setContentType("text/html");
        printWriter.println("The employee is updated succesfully:<br>");
        int id = Integer.parseInt(req.getParameter("empId"));
        String ename = req.getParameter("empName");
        String enumberupdate = req.getParameter("empNumber");
        employeeDaoImp.updateEmployee(id, ename, enumberupdate);


        RequestDispatcher rd = req.getRequestDispatcher("show.html");
        rd.include(req, resp);


    }
}
