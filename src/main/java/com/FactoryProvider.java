package com;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class FactoryProvider {
    public  static SessionFactory sessionFactory;
    public static SessionFactory getFactory()
    {
        if (sessionFactory==null)
        {
            Configuration configuration = new Configuration().configure();
            sessionFactory= configuration.buildSessionFactory();
        }
        return sessionFactory;
    }
}
