package com;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

@WebServlet("/AddEmployeeServlet")
public class AddEmployeeServlet extends HttpServlet {



    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter printWriter=resp.getWriter();
        resp.setContentType("text/html");

        EmployeeDetails employeeDetails=new EmployeeDetails();
        EmployeeDaoImp employeeDaoImp=new EmployeeDaoImp();

            int empId=Integer.parseInt(req.getParameter("empId"));
            String ename = req.getParameter("empName");
            String enumber = req.getParameter("empNumber");
            employeeDetails.setEmployeeId(empId);
            employeeDetails.setEmpName(ename);
            employeeDetails.setEmpNumber(enumber);
            employeeDaoImp.saveEmployee(employeeDetails);
            RequestDispatcher rd = req.getRequestDispatcher("show.html");
            rd.include(req, resp);

    }




}
