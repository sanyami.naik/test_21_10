package com;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
@WebServlet("/DeleteServlet")
public class DeleteServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        EmployeeDaoImp employeeDaoImp=new EmployeeDaoImp();
        EmployeeDetails employee=new EmployeeDetails();
        PrintWriter printWriter=resp.getWriter();
        resp.setContentType("text/html");
        printWriter.println("The employee is updated succesfully:<br>");
        int empId=Integer.parseInt(req.getParameter("empId"));
        employee.setEmployeeId(empId);
        employeeDaoImp.deleteEmployee(employee);

        RequestDispatcher rd = req.getRequestDispatcher("show.html");
        rd.include(req, resp);

    }
}
