package com;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class EmployeeDaoImp implements EmployeeDao {
    public void saveEmployee(EmployeeDetails employee) {

        SessionFactory sessionFactory=FactoryProvider.getFactory();
        Session session=sessionFactory.openSession();
        Transaction transaction= session.beginTransaction();


        session.save(employee);
        transaction.commit();
        session.close();
    }

    public List<EmployeeDetails> showAllEmployee() {
        List<EmployeeDetails> employeeDetailsList=new ArrayList();
        SessionFactory sessionFactory=FactoryProvider.getFactory();
        Session session=sessionFactory.openSession();


        Query query=session.createQuery("from EmployeeDetails");
        employeeDetailsList= query.list();
        session.close();
        return employeeDetailsList;

    }

    public void updateEmployee(int id, String name, String number) {

        SessionFactory sessionFactory=FactoryProvider.getFactory();
        Session session=sessionFactory.openSession();
        Transaction transaction=session.beginTransaction();

        EmployeeDetails employeeDetails1=session.load(EmployeeDetails.class,id);
        employeeDetails1.setEmpName(name);
        employeeDetails1.setEmpNumber(number);

        session.update(employeeDetails1);
        transaction.commit();
        session.close();
    }

    public void deleteEmployee(EmployeeDetails employee) {
        SessionFactory sessionFactory=FactoryProvider.getFactory();
        Session session=sessionFactory.openSession();
        Transaction transaction=session.beginTransaction();
        session.delete(employee);
        transaction.commit();
        session.close();

    }
}
